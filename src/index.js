import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
// components
import './index.css';
import App from './App';
// service worker for PWA
import * as serviceWorker from './serviceWorker';
// store
import reducers from './App/reducers';

const store = createStore(reducers);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>, 
    document.getElementById('app')
);

serviceWorker.unregister();
