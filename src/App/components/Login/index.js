import React, {Component} from 'react';

export default ({ onLogin }) => {
        return (
            <form className="col-md-4 offset-md-4" 
                  onSubmit={(e) => onLogin(
                      document.getElementById("username").value, 
                      document.getElementById("password").value, 
                      e)}>
                <div className="form-group">
                    <input type="text" 
                           className="form-control" 
                           placeholder="Enter username" 
                           id="username"/>
                </div>
                <div className="form-group">
                    <input type="password" 
                           className="form-control" 
                           placeholder="Password" 
                           id="password"/>
                </div>
                <button type="submit" className="btn btn-primary">Submit</button>
            </form>
        )
}