import React from 'react';

const News = ({ news, deleteNews }) => {
    return (
        <div className="news container">
            <div className="row">
                {news.map((article, i) => 
                    <div key={i} className="card col-md-4">
                        <img src={article.image} className="card-img-top" alt={article.title} />
                        <div className="card-body">
                            <h5 className="card-title">{article.title}</h5>
                            <p className="card-text">{article.content.substr(0, 100) + "..."}</p>
                            <button type="button" 
                                    className="btn btn-danger"
                                    onClick={() => deleteNews(article)}>Delete</button>
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
}
export default News;