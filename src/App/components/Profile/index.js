import React from 'react';

const Profile = ({ onLogout }) => {
    if (!localStorage.getItem("login")) {
        window.location.href = "/login";
    } else {
        return (
            <div>
                Wow! You getted access for the page. <button onClick={onLogout}>Logout</button>
            </div>
        )
    }
}
export default Profile;