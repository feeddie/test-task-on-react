const users = [
    {
        username: "Admin",
        password: "123456"
    }
]

export default function (state=users, action) {
    switch(action.type) {
        case "LOGIN": return state.map(user => {
            let { username, password } = action.payload;
            if (user.username === username && user.password === password) {
                localStorage.setItem("login", "_token is here");
                window.location.href = "/profile";
            } else {
                // можно сделать проверку на уровне GET
                window.location.href = "/login?message=такого пользователя не существует";
            }
        });
        default: return state;
    }
}