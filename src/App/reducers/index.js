import { combineReducers } from 'redux';
// reducers
import news from './news';
import login from './login';

export default combineReducers({news, login});