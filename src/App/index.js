import React, { Component} from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route } from 'react-router-dom';
// components
import Nav from './components/Nav';
// pages
import Home from './components/Home';
import News from './components/News';
import Profile from './components/Profile';
import Login from './components/Login';
// actions

class App extends Component {
  deleteNews = article => {
    let { deleteNews } = this.props;
    deleteNews(article.id);
  }

  addNews = () => {
    // можно дописать реализациюю
    // в ТЗ не было требований к CRUD
    // поэтому я реализовал удаление, а добавление
    // на уровне Redux
    let { addNews } = this.props;
    addNews({});
  }

  onLogin = (username, password, event) => {
    event.preventDefault();
    let user = {
      username,
      password
    }

    this.props.logIn(user);
    return false;
  }

  onLogout = () => {
    localStorage.removeItem("login");
    window.location.href = "/";
  }

  render() {
    let { news } = this.props;
    
    return (
      <div className="container">
        <Router>
          <Nav />
          <main className="content">
            <Route exact path="/" component={Home} />
            <Route path="/news/" component={() => 
              <News news={news}  
                    deleteNews={this.deleteNews}
              />} 
            />
            <Route path="/profile/" component={() => 
              <Profile onLogout={this.onLogout} />
            } />
            <Route path="/login/" component={() =>
              <Login onLogin={this.onLogin} />
            } />
          </main>
        </Router>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    news: state.news
  }
}

function matchDispatchToProps(dispatch) {
  return {
    addNews: article => {
      dispatch({ type: "ADD_NEWS", payload: article })
    },
    deleteNews: newsId => {
      dispatch({ type: "DELETE_NEWS", payload: newsId });
    },
    logIn: user => {
      dispatch({ type: "LOGIN", payload: user })
    }
  }
}

export default connect(mapStateToProps, matchDispatchToProps)(App);