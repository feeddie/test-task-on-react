Это практическое приложения для оттачивания знаний в React и Redux.

## Суть приложения

В приложении реализованы основные страницы:
- Главная (/)
- Новости (/news)
- Профиль (/profile)
- Авторизация (/login)

### Использовались следующие инструменты
- React and React-dom
- Redux and React-redux
- React-router

### Суть действующего ПО

На сайте есть главная страница и новости, которые доступны всем пользователям. А также есть страница профиля, которая доступна только авторизованным пользователям. Когда вы заходите на страницу "Profile", вас отправляет на страницу авторизации. В данный момент авторизация фикисрована и работает на одного пользователя:
username: "Admin"
password: "123456"

Спасибо за внимание :) 